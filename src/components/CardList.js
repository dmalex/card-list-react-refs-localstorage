//
// Dmitry Alexandrov
// B.RF Group
//
import React, { useState, useRef, useEffect } from "react"
import ListItem from "./ListItem"

export default function CardList() {
  //localStorage.clear()
  var objects
  const items = localStorage.getItem("cards")
  if (items) {
    objects = JSON.parse(items)
  }
  const [cards, setCards] = useState(objects ? objects : [])
  const textInput = useRef(null) // реф сможет иметь доступ
  const [text, setText] = useState("")
  const [card, setCard] = useState()
  
  useEffect(() => {
    saveLocal()
  }, [cards])
 

  function saveLocal(item = "cards") {
    if (cards) {
      localStorage.setItem(item, JSON.stringify(cards))
      // const its = localStorage.getItem(item)
      // alert(its)
    }
  }

  function handleInput(e) { // edit text
    setText(e.target.value) // save entered text
  }

  function handleCancel() { // cancel text input    
    setCard() // clear saved card
    setText("") // clear text
    textInput.current.focus()
  }

  function handleSave() { // save changed card
    const editedCard = { ...card, text: text }
    setCards([editedCard, ...cards.filter((c) => c.id !== card.id)])
    // saveLocal()
    handleCancel()
  }

  function handleAdd() { // add or change card
    const newCard = {
      id: "_" + Math.random().toString(36).substring(2, 9),
      text: text
    }
    setCards([newCard, ...cards]) // save new card on top
    // saveLocal()
    setText("") // clear text
  }

  function handleRemove(id) { // remove the card
    setCards(cards.filter((c) => c.id !== id))
    // saveLocal()
    handleCancel()
  }

  function handleClick(id) { // click on card
    if (card && card.id === id) {
      handleCancel()
    } else { // edit the card
      const card = cards.filter((c) => c.id === id)[0]
      setCard(card) // save the card for changing
      setText(card.text) // place text from the card to change
      textInput.current.focus()
    }
  }

  return (
    <div className="app">
      <form className="add-card"> 
        <label>
          { card ? <p>Правка текста карточки</p> :
                   <p>Введите текст для добавления карточки</p>}

          <textarea value={text} onInput={handleInput}
                    rows="5" ref={textInput} />
        </label>
        <div className="buttons">
          {(text && !card) &&
          <button class="button" type="submit"
          onClick={handleAdd}>Добавить карточку</button>}

          {(card && card.text !== text && text.length > 0) &&
          <button class="button" type="submit"
          onClick={handleSave}>Сохранить правку</button>}

          {(card) &&
          <button class="button" type="submit"
          onClick={() => handleRemove(card.id)}>Удалить карточку</button>}

          {(text || (card && text.length === 0)) &&
          <button class="button" type="cancel"
          onClick={handleCancel}>Отмена</button>}
        </div>
      </form>

      <div className="card-list">
        {cards.map((c) => (
          <ListItem key={c.id} card={c} selected={c === card ? true : false}
                    onClick={handleClick}/>
        ))}
      </div>
    </div>
  )
}
