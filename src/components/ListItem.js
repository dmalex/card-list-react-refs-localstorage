//
// Dmitry Alexandrov
// B.RF Group
//
import React, { useRef } from "react"

export default function ListItem({ card, selected, onClick }) {
  const currentCard = useRef(null) // реф сможет иметь доступ

  let cname = "card-item"
  if (selected === true) {
    cname = "card-selected-item"
  }

  return (
    <div className={cname} ref={currentCard} onClick={() => onClick(card.id)}>
      <p>{card.text}</p>
    </div>
  )
}
